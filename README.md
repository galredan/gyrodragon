Prérequis pour lancer les scripts :
- Ubuntu version 16.04
- PostgreSQL
- PGAdmin4 version 1.3
- Python 2 à minima (pour installer PGAdmin4)
- Un utilisateur PostgreSQL ayant les pleins pouvoir (nous avons choisit l'utilisateur "appli_web" par défaut,
    ainsi que la database "appli_web") 
    cependant, tous les scripts demandent une entrée utilisateur pour choisir :
    - les directories (à cause d'une arborescence un peu différente avec les paramètres par défauts de Parallels Desktop)
    - les utilisateurs
    - les hôtes
    - les noms de bases de données
    
Il faudra avoir installé et configuré ces différents services pour pouvoir utiliser les scripts.
Vous retrouverez dans la documentation comment initialiser votre espace de travail.
Vous serez également guidés pour vous connecter aux services dont nous auront besoin.

Afin d'exécuter un script, lorsque vous êtes connecté à votre base de données, utilisez la commande "sh scriptToExecute"

De plus, nous avons référencé dans la documentation la mise en place d'une machine Debian 9 avec MySQL, PHPmyAdmin et Apache
que nous avions initialement faite avant de choisir de repartir sur du PostgreSQL.