#!/bin/bash

echo "Choisissez le dossier où sauvegarder"
read directory
echo "Entrez le nom de la database"
read db
echo "Entrez votre nom d'utilisateur"
read user

mkdir $directory `date +\%Y\%m\%d`
pg_dump -U $user $db | gzip > /home/parallels/Desktop/`date +\%Y\%m\%d`/$db_FULL.sql.gz
pg_dump -s -U $user $db | gzip > /home/parallels/Desktop/`date +\%Y\%m\%d`/$db_SCHEMA.sql.gz
pg_dump -a -U $user $db | gzip > /home/parallels/Desktop/`date +\%Y\%m\%d`/$db_DATA.sql.gz

echo "--------------"
echo "Une sauvegarde a été effectuée dans $directory"
exit

