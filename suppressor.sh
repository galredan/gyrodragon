#!/bin/bash

echo "Choisissez la sauvegarde à supprimer"
read save
echo "Choisissez la durée avant suppression (en jours)"
read duration

if [ -d "$save" ]; then
find "$data" type f -mtime +$duration -name "*.dump" -execdir rm -- '{}' \;
else
echo "--------------"
echo "Répertoire non trouvé"
exit
fi
exit
