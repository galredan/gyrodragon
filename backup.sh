#!/bin/bash

echo "Entrez le chemin de la sauvegarde à restaurer"
read directory
echo "Choisissez quelle database vous souhaitez restaurer"
read db
echo "Entrez votre nom d'utilisateur"
read user
echo "Entrez le nom de l'hôte"
read host

if [ -d "$directory" ];
then
cd "$directory"
ls
for file in *.dump;
do
pg_restore -h $host -U $user -c -v -d "$db" "$file"
done
echo "--------------"
echo "La restauration à partir de $directory a bien été effectuée"
else
echo "--------------"
echo "Répertoire non trouvé"
exit
fi
exit
